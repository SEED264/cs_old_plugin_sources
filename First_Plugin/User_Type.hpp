#pragma once
#include "TriglavPlugInSDK\TriglavPlugInSDK.h"
#include<CL\cl.hpp>

typedef unsigned char byte;

struct FP_FilterInfo {
	int amount;
	float aspect;
	int blurType;
	int sampling;
	int divideSise;

	TriglavPlugInPropertyService *PService;
	TriglavPlugInPropertyService2 *PService2;
};

cl_device_type kernelDeviceType[] = {CL_DEVICE_TYPE_GPU, CL_DEVICE_TYPE_CPU};

cl_addressing_mode addressModes[] = {CL_ADDRESS_MIRRORED_REPEAT, CL_ADDRESS_CLAMP};
