#pragma once

#include <TriglavPlugInSDK\TriglavPlugInSDK.h>
#include <CL\cl.hpp>
#include <Windows.h>
#include <string>
#include <map>
#include <array>
#include <functional>
using namespace std;
#ifdef _DEBUG_
#define CP_debug_s(x) OutputDebugStringA(x)
#else
#define CP_debug_s(x)
#endif

#ifdef _DEBUG_
#define debugCode(x) x
#else
#define debugCode(x)
#endif


namespace CPUtil {

	struct Vec2 {
		int x;
		int y;
		Vec2() {
			Vec2(0, 0);
		}
		Vec2(int x, int y){
			this->x = x;
			this->y = y;
		}
	};

	struct Size2D{
		unsigned int w;
		unsigned int h;
		Size2D() {
			Size2D(0, 0);
		}
		Size2D(unsigned int w, unsigned int h) {
			this->w = w;
			this->h = h;
		}
		inline unsigned int area() {
			return w*h;
		};
	};

	struct Bounds2D {
		Vec2 origin;
		Size2D size;
		Bounds2D(){
			Bounds2D(Vec2(), Size2D());
		}
		Bounds2D(Vec2 origin, Size2D size) {
			this->origin = origin;
			this->size = size;
		}
	};

	void getStrObj(TriglavPlugInServer *pServer, TriglavPlugInStringObject &strObj, TriglavPlugInInt strID);

	void createStrObj(TriglavPlugInServer *pServer, TriglavPlugInStringObject &strObj, string str);

	Bounds2D calcBounds(Size2D imgSize, TriglavPlugInRect pArea, Size2D extendSize);

	vector<TriglavPlugInRect> calcDividedRects(Size2D srcSize, Size2D divSize, Size2D *block2DNum = nullptr);

	vector<float> calcFactor(int blurAmount, bool isGaussian);
}