#pragma once
#include <string>
#include <chrono>
using namespace std;

bool contain(string srcstr, string contstr);

template<class T = chrono::milliseconds> double calcDuration(chrono::system_clock::time_point start,
	chrono::system_clock::time_point end) {
	return chrono::duration_cast<T>(end - start).count();
}