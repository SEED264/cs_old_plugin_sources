#include <TriglavPlugInSDK\TriglavPlugInSDK.h>
#include <CL\cl.hpp>
#include <string>
#include <vector>
#include <chrono>
#include <opencv2\world.hpp>
#include <opencv2\imgcodecs.hpp>
#include <Windows.h>
#include <fstream>
#include "CL_UtilFunc.hpp"
#include "CP_Util.hpp"
#include "User_Func.hpp"
#include "User_Type.hpp"
#include "UtilFunc.hpp"
string kernelcode = ""
#include "kernel.cl"
;
using namespace std;
using namespace CPUtil;

const unsigned int amountKey = 1;
const unsigned int aspectKey = 2;
const unsigned int samplingKey = 3;
const unsigned int bTypeKey = 4;
const unsigned int divideKey = 5;
const unsigned int resetKey = 6;

const unsigned int categoryName_res = 101;
const unsigned int filterName_res = 102;
const unsigned int amountCaption_res = 103;
const unsigned int aspectcaption_res = 104;

bool cl_builded = false;

static void TRIGLAV_PLUGIN_CALLBACK TriglavPlugInFilterPropertyCallBack(TriglavPlugInInt* result, TriglavPlugInPropertyObject propertyObject,
																		const TriglavPlugInInt itemKey, const TriglavPlugInInt notify, TriglavPlugInPtr data) {
	(*result) = kTriglavPlugInPropertyCallBackResultNoModify;
	FP_FilterInfo *filterInfo = (FP_FilterInfo*)data;
	if (filterInfo!=nullptr) {
		TriglavPlugInPropertyService *propService = filterInfo->PService;
		TriglavPlugInPropertyService2 *propService2 = filterInfo->PService2;
		if (propService!=NULL) {
			if (notify==kTriglavPlugInPropertyCallBackNotifyValueChanged) {

				if (itemKey==amountKey) {
					TriglavPlugInInt value;
					propService->getIntegerValueProc(&value, propertyObject, itemKey);
					if (filterInfo->amount!=value) {
						filterInfo->amount = value;
						*result = kTriglavPlugInPropertyCallBackResultModify;
					}
				}
				else if (itemKey == aspectKey) {
					TriglavPlugInDouble value;
					propService->getDecimalValueProc(&value, propertyObject, itemKey);
					if (filterInfo->aspect != value) {
						filterInfo->aspect = value;
						*result = kTriglavPlugInPropertyCallBackResultModify;
					}
				}
				else if (itemKey == samplingKey) {
					TriglavPlugInInt value;
					propService2->getEnumerationValueProc(&value, propertyObject, samplingKey);
					if (filterInfo->sampling != value) {
						filterInfo->sampling = value;
						*result = kTriglavPlugInPropertyCallBackResultModify;
					}
				}
				else if (itemKey == bTypeKey) {
					TriglavPlugInInt value;
					propService2->getEnumerationValueProc(&value, propertyObject, bTypeKey);
					if (filterInfo->blurType != value) {
						filterInfo->blurType = value;
						*result = kTriglavPlugInPropertyCallBackResultModify;
					}
				}
				else if (itemKey == divideKey) {
					TriglavPlugInInt value;
					propService2->getEnumerationValueProc(&value, propertyObject, divideKey);
					CP_debug_s(("Enum check : " + to_string(value)).c_str());
					if (filterInfo->divideSise != value) {
						filterInfo->divideSise = value;
						*result = kTriglavPlugInPropertyCallBackResultModify;
					}
				}
			}
			else if (notify==kTriglavPlugInPropertyCallBackNotifyButtonPushed)
			{
				if (itemKey == resetKey) {
					cl_builded = false;
					CP_debug_s(("Pushed key : " + to_string(itemKey)).c_str());
					*result = kTriglavPlugInPropertyCallBackResultModify;
				}
			}
		}
	}
}
vector<cl::Platform> platformList;
cl::Device device;
cl::Context context;
cl::CommandQueue commandqueue;
cl::Program program;
cl::Kernel kernel;
cl::Image2D inputImage;
cl::Image2D outputImage;
cl::Image2D tempImage;
cl::Image2D srcImage;
cl::Image2D dstImage;


void TRIGLAV_PLUGIN_API TriglavPluginCall(TriglavPlugInInt* result, TriglavPlugInPtr* data, TriglavPlugInInt selector,
										  TriglavPlugInServer* pluginServer, TriglavPlugInPtr reserved) {
	// resultの初期値 処理が正常に終了した場合はその時にSuccessが代入される
	*result = kTriglavPlugInCallResultFailed;

	// selectorの値に応じて処理を行う
	// モジュール、フィルターの初期化、終了処理とフィルタの実行処理をそれぞれ実装する
	if (pluginServer != NULL) {

		switch (selector) {
				// モジュールの初期化処理
			case kTriglavPlugInSelectorModuleInitialize: {
				CP_debug_s("Debug version");
				CP_debug_s("Plugin initializing");
				TriglavPlugInModuleInitializeRecord *MIRecord = pluginServer->recordSuite.moduleInitializeRecord;
				TriglavPlugInStringService *sService = pluginServer->serviceSuite.stringService;
				// ホストのバージョン取得
				TriglavPlugInInt HostVersion;
				MIRecord->getHostVersionProc(&HostVersion, pluginServer->hostObject);
//				OutputDebugStringA(("CLIP STUDIO PAINT version : " + to_string(HostVersion)).c_str());
//				OutputDebugStringA(("Required Host version : " + to_string(kTriglavPlugInNeedHostVersion)).c_str());
				if (HostVersion >= kTriglavPlugInNeedHostVersion) {
					// moduleIDの設定
					TriglavPlugInStringObject moduleID = NULL;
					string mIDstr = "F9B8C20E-ADE2-4045-94D4-E51FBB4957B5";
					sService->createWithAsciiStringProc(&moduleID, mIDstr.c_str(), mIDstr.length());
					MIRecord->setModuleIDProc(pluginServer->hostObject, moduleID);
					// フィルタの種類の設定
					MIRecord->setModuleKindProc(pluginServer->hostObject, kTriglavPlugInModuleSwitchKindFilter);
					sService->releaseProc(moduleID);
					// プラグインで使うデータ用構造体の作成
					FP_FilterInfo *FilterInfo = new FP_FilterInfo;
					*data = FilterInfo;

					*result = kTriglavPlugInCallResultSuccess;
				};
				break;
			}
			// モジュールの終了処理
			case kTriglavPlugInSelectorModuleTerminate: {
				CP_debug_s("Plugin terminate");
				// 初期化処理で確保したデータの開放
				FP_FilterInfo *FilterInfo = (FP_FilterInfo*)*data;
				delete FilterInfo;
				*data = nullptr;
				*result = kTriglavPlugInCallResultSuccess;
				break;
			}

														// フィルタの初期化処理
			case kTriglavPlugInSelectorFilterInitialize: {
				CP_debug_s("Filter initializing");
				TriglavPlugInRecordSuite *RecordSuite = &pluginServer->recordSuite;
				TriglavPlugInHostObject hostObject = pluginServer->hostObject;
				TriglavPlugInStringService *strService = pluginServer->serviceSuite.stringService;
				TriglavPlugInPropertyService *propService = pluginServer->serviceSuite.propertyService;
				TriglavPlugInPropertyService2 *propService2 = pluginServer->serviceSuite.propertyService2;
				if (TriglavPlugInGetFilterInitializeRecord(RecordSuite) != NULL && strService != NULL && propService != NULL) {

					TriglavPlugInStringObject catName = nullptr;
					TriglavPlugInStringObject filterName = nullptr;
					// リソーステーブルに定義した文字列を取得
					createStrObj(pluginServer, catName, "OpenCL Effects");
					createStrObj(pluginServer, filterName, "Blur");
					// 取得した文字列をそれぞれ名前として設定
					TriglavPlugInFilterInitializeSetFilterCategoryName(RecordSuite, hostObject, catName, 'c');
					TriglavPlugInFilterInitializeSetFilterName(RecordSuite, hostObject, filterName, 'h');
					// 用済みの文字列オブジェクトを開放
					strService->releaseProc(catName);
					strService->releaseProc(filterName);
					// フィルタを実行するターゲットの指定
					TriglavPlugInInt target[] = { kTriglavPlugInFilterTargetKindRasterLayerRGBAlpha };
					TriglavPlugInFilterInitializeSetTargetKinds(RecordSuite, hostObject, target, 1);
					// プレビューの表示
					TriglavPlugInFilterInitializeSetCanPreview(RecordSuite, hostObject, true);

					TriglavPlugInPropertyObject propObject;
					propService->createProc(&propObject);

					CP_debug_s("Set Amount parameter.");
					// amount
					TriglavPlugInStringObject amountCaption = nullptr;
					createStrObj(pluginServer, amountCaption, "ブラー半径(px)");
					propService->addItemProc(propObject, amountKey, kTriglavPlugInPropertyValueTypeInteger, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindDefault, amountCaption, 'm');
					propService->setIntegerValueProc(propObject, amountKey, 0);
					propService->setIntegerDefaultValueProc(propObject, amountKey, 0);
					propService->setIntegerMinValueProc(propObject, amountKey, 0.0);
					propService->setIntegerMaxValueProc(propObject, amountKey, 1000.0);
					strService->releaseProc(amountCaption);

					CP_debug_s("Set Aspect parameter.");
					// aspect
					TriglavPlugInStringObject aspectCaption = nullptr;
					createStrObj(pluginServer, aspectCaption, "縦横比(%)");
					propService->addItemProc(propObject, aspectKey, kTriglavPlugInPropertyValueTypeDecimal, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindDefault, aspectCaption, 'a');
					propService->setDecimalValueProc(propObject, aspectKey, 0.0);
					propService->setDecimalDefaultValueProc(propObject, aspectKey, 0.0);
					propService->setDecimalMinValueProc(propObject, aspectKey, -100.0);
					propService->setDecimalMaxValueProc(propObject, aspectKey, 100.0);
					strService->releaseProc(aspectCaption);

					CP_debug_s("Set Sampling Mode parameter.");
					// sampling mode
					TriglavPlugInStringObject samplingCaption = nullptr;
					TriglavPlugInStringObject choiceCaption = nullptr;
					createStrObj(pluginServer, samplingCaption, "範囲外サンプリングモード");
					propService->addItemProc(propObject, samplingKey, kTriglavPlugInPropertyValueTypeEnumeration, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindDefault, samplingCaption, 's');
					propService2->setEnumerationDefaultValueProc(propObject, samplingKey, 0);
					propService2->setEnumerationValueProc(propObject, samplingKey, 0);
					createStrObj(pluginServer, choiceCaption, "ミラー");
					propService2->addEnumerationItemProc(propObject, samplingKey, 0, choiceCaption, 'm');
					createStrObj(pluginServer, choiceCaption, "無し");
					propService2->addEnumerationItemProc(propObject, samplingKey, 1, choiceCaption, 'n');
					strService->releaseProc(samplingCaption);
					strService->releaseProc(choiceCaption);

					CP_debug_s("Set Blur Type parameter.");
					// Blur Type
					TriglavPlugInStringObject blurTypeCaption = nullptr;
					createStrObj(pluginServer, blurTypeCaption, "ブラーの種類");
					propService->addItemProc(propObject, bTypeKey, kTriglavPlugInPropertyValueTypeEnumeration, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindDefault, blurTypeCaption, 's');
					propService2->setEnumerationDefaultValueProc(propObject, bTypeKey, 1);
					propService2->setEnumerationValueProc(propObject, bTypeKey, 1);
					createStrObj(pluginServer, choiceCaption, "ボックス");
					propService2->addEnumerationItemProc(propObject, bTypeKey, 0, choiceCaption, 'b');
					createStrObj(pluginServer, choiceCaption, "ガウス");
					propService2->addEnumerationItemProc(propObject, bTypeKey, 1, choiceCaption, 'g');
					strService->releaseProc(samplingCaption);
					strService->releaseProc(choiceCaption);

					CP_debug_s("Set Divide Size parameter.");
					// Divide Size
					TriglavPlugInStringObject divideCaption = nullptr;
					createStrObj(pluginServer, divideCaption, "分割サイズ(px)");
					propService->addItemProc(propObject, divideKey, kTriglavPlugInPropertyValueTypeEnumeration, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindDefault, divideCaption, 'd');
					propService2->setEnumerationDefaultValueProc(propObject, divideKey, 512);
					propService2->setEnumerationValueProc(propObject, divideKey, 512);
					createStrObj(pluginServer, choiceCaption, "512x512");
					propService2->addEnumerationItemProc(propObject, divideKey, 512, choiceCaption, '5');
					createStrObj(pluginServer, choiceCaption, "1024x1024");
					propService2->addEnumerationItemProc(propObject, divideKey, 1024, choiceCaption, '1');
					createStrObj(pluginServer, choiceCaption, "2048x2048");
					propService2->addEnumerationItemProc(propObject, divideKey, 2048, choiceCaption, '2');
					createStrObj(pluginServer, choiceCaption, "4096x4096");
					propService2->addEnumerationItemProc(propObject, divideKey, 4096, choiceCaption, '4');
					strService->releaseProc(divideCaption);
					strService->releaseProc(choiceCaption);

					CP_debug_s("Store parameter.");
					propService2->setItemStoreValueProc(propObject, amountKey, true);
					propService2->setItemStoreValueProc(propObject, aspectKey, true);
					propService2->setItemStoreValueProc(propObject, samplingKey, true);
					propService2->setItemStoreValueProc(propObject, divideKey, true);


					CP_debug_s("Set Reset button.");
					// Reset Button
					TriglavPlugInStringObject resetCaption = nullptr;
					createStrObj(pluginServer, resetCaption, "OpenCLリセット");
					propService->addItemProc(propObject, resetKey, kTriglavPlugInPropertyValueTypeVoid, kTriglavPlugInPropertyValueKindDefault, kTriglavPlugInPropertyInputKindPushButton, resetCaption, 'r');
					strService->releaseProc(resetCaption);
					

					// プロパティの設定
					CP_debug_s("TriglavPlugInFilterInitializeSetProperty");
					TriglavPlugInFilterInitializeSetProperty(RecordSuite, hostObject, propObject);
					CP_debug_s("TriglavPlugInFilterInitializeSetPropertyCallBack");
					TriglavPlugInFilterInitializeSetPropertyCallBack(RecordSuite, hostObject, TriglavPlugInFilterPropertyCallBack, *data);
					CP_debug_s("Filter has Initialized.");
					*result = kTriglavPlugInCallResultSuccess;
				}
				break;
			}

														 // フィルタの終了処理
			case kTriglavPlugInSelectorFilterTerminate: {
				CP_debug_s("Filter terminate");
				*result = kTriglavPlugInCallResultSuccess;
				break;
			}

														// フィルタの実行処理
			case kTriglavPlugInSelectorFilterRun: {
				CP_debug_s("Filter running");
				TriglavPlugInHostObject hostObject = pluginServer->hostObject;
				TriglavPlugInRecordSuite *recordSuite = &pluginServer->recordSuite;
				TriglavPlugInOffscreenService *offscreenService = pluginServer->serviceSuite.offscreenService;
				TriglavPlugInOffscreenService2 *offscreenService2 = pluginServer->serviceSuite.offscreenService2;
				TriglavPlugInBitmapService *bitmapService = pluginServer->serviceSuite.bitmapService;
				TriglavPlugInPropertyService *propService = pluginServer->serviceSuite.propertyService;
				TriglavPlugInPropertyService2 *propService2 = pluginServer->serviceSuite.propertyService2;
				if (TriglavPlugInGetFilterRunRecord(recordSuite) != nullptr&&offscreenService != nullptr&&propService != nullptr) {
					// プロパティオブジェクトの取得
					TriglavPlugInPropertyObject propObject;
					TriglavPlugInFilterRunGetProperty(recordSuite, &propObject, hostObject);

					// ソースのオフスクリーンオブジェクトを取得
					TriglavPlugInOffscreenObject srcOffscreenObject;
					TriglavPlugInFilterRunGetSourceOffscreen(recordSuite, &srcOffscreenObject, hostObject);

					// 実行先のオフスクリーンオブジェクトを取得
					TriglavPlugInOffscreenObject dstOffscreenObject;
					TriglavPlugInFilterRunGetDestinationOffscreen(recordSuite, &dstOffscreenObject, hostObject);

					// 選択範囲を取得
					TriglavPlugInRect selectAreaRect;
					TriglavPlugInFilterRunGetSelectAreaRect(recordSuite, &selectAreaRect, hostObject);

					// 選択範囲内のオフスクリーンを取得
					TriglavPlugInOffscreenObject selectAreaOffscreenObject;
					TriglavPlugInFilterRunGetSelectAreaOffscreen(recordSuite, &selectAreaOffscreenObject, hostObject);

					// RGBインデックスの取得
					TriglavPlugInInt r, g, b;
					offscreenService->getRGBChannelIndexProc(&r, &g, &b, dstOffscreenObject);

					// ブロックの数を取得
					TriglavPlugInInt blockRectCount;
					offscreenService->getBlockRectCountProc(&blockRectCount, dstOffscreenObject, &selectAreaRect);

					vector<TriglavPlugInRect> blockRects;
					blockRects.resize(blockRectCount);
					for (size_t i = 0; i < blockRectCount; i++) {
						offscreenService->getBlockRectProc(&blockRects[i], i, dstOffscreenObject, &selectAreaRect);
					}

					FP_FilterInfo *filterInfo = (FP_FilterInfo*)*data;
					filterInfo->PService = propService;
					filterInfo->PService2 = propService2;

					Size2D imgSize(selectAreaRect.right-selectAreaRect.left,
									  selectAreaRect.bottom-selectAreaRect.top);

					TriglavPlugInPoint zeroPos = {0,0};

					Size2D screenSize;
					offscreenService->getWidthProc((TriglavPlugInInt*)&screenSize.w, srcOffscreenObject);
					offscreenService->getHeightProc((TriglavPlugInInt*)&screenSize.h, srcOffscreenObject);

					TriglavPlugInFilterRunSetProgressTotal(recordSuite, pluginServer->hostObject, 1);

					TriglavPlugInInt co;
					offscreenService->getChannelOrderProc(&co, dstOffscreenObject);
					TriglavPlugInBitmapObject bitmap, dstbitmap;
					byte *bmpptr, *dstbmpptr;
					TriglavPlugInPtr pt, dstpt;
					bitmapService->createProc(&bitmap, screenSize.w, screenSize.h, 4, kTriglavPlugInBitmapScanlineHorizontalLeftTop);
					offscreenService->getBitmapProc(bitmap, &zeroPos, srcOffscreenObject, &zeroPos, screenSize.w, screenSize.h, kTriglavPlugInOffscreenCopyModeNormal);
					bitmapService->getAddressProc(&pt, bitmap, &zeroPos);
					bmpptr = (byte*)pt;

					TriglavPlugInInt alpIndex;
					pluginServer->serviceSuite.offscreenService2->getBitmapNormalAlphaChannelIndexProc(&alpIndex, srcOffscreenObject);
					//CP_debug_s(("Alpha index : " + to_string(alpIndex)).c_str());


					cl::size_t<3> origin = CLFunc::createSize_t({ 0, 0, 0 });
					cl::size_t<3> region = CLFunc::createSize_t({ (int)screenSize.w, (int)screenSize.h, 1 });

					bool restart = true;
					bool allocImage = true;
					bool allocDstImage = true;
					bool isGaussian = true;
					int amount_x = 0;
					int amount_y = 0;
					int cDiv = 0;
					size_t maxWorkSize = 0;
					cl::Sampler sampler;
					int usedDevice = 100;
					TriglavPlugInInt blockIndex = 0;
					int err = -30;
					CPUtil::Size2D dstSize(screenSize);
					Size2D divSize(screenSize);
					vector<TriglavPlugInRect> blocks = CPUtil::calcDividedRects(screenSize, screenSize);
					int blockNum = blocks.size();
					cl::ImageFormat ioImgFormat(CL_RGBA, CL_UNORM_INT8);
					cl::ImageFormat tempImgFormat(CL_RGBA, CL_FLOAT);

					while (true) {
						if (!cl_builded) {/*
							ifstream s("C:\\Users\\SEED264\\Documents\\CELSYS\\CLIPStudioModule\\PlugIn\\PAINT\\k\\kernel.cl");
							if (s){
								CP_debug_s("kernel loaded.");
							}
							stringstream s2;
							s2 << s.rdbuf();
							kernelcode = s2.str();*/

							CP_debug_s("OpenCL Initilizinng");
							chrono::system_clock::time_point start, end;
							double dur = 0;
							auto now = chrono::system_clock::now;
							debugCode(
								start = now();
								);
							cl::Platform::get(&platformList);
							debugCode(
								dur = calcDuration<chrono::nanoseconds>(start, now()) / 1000000;
								CP_debug_s(("Get Platform Time : " + to_string(dur) + "ms").c_str());
								start = now();
								);
							CLFunc::sortbydGPU(platformList);
							debugCode(
								dur = calcDuration<chrono::nanoseconds>(start, now()) / 1000000;
								CP_debug_s(("Sort Platform Time : " + to_string(dur) + "ms").c_str());
								);
							device = CLFunc::chooseDevice(platformList, CL_DEVICE_TYPE_GPU);
							CP_debug_s(("Using device : " + CLFunc::getDeviceName(device)).c_str());
							context = cl::Context(device);
							commandqueue = cl::CommandQueue(context, device);
							program = cl::Program(context, kernelcode, false);
							vector<cl::Device> buildList = { device };
							program.build(buildList);
							CP_debug_s(("Build Log : " + program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device)).c_str());
							kernel = cl::Kernel(program, "func");
							CP_debug_s(("MAX constant buffer size : " + to_string(device.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>())).c_str());
//							CP_debug_s("Create Image");
							cl_builded = true;
							allocImage = true;

							//vector<size_t> ls =  device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
							//CP_debug_s(("Local size : " + to_string(ls[0]) + " : " + to_string(ls[1]) + " : " + to_string(ls[2]) + " : ").c_str());
							//commandqueue.enqueueMapImage(srcImage, true, CL_MEM_READ_WRITE, origin, region, 0, 0);
							CP_debug_s("Image mapped");
							//commandqueue.enqueueWriteImage(srcImage, true, origin, region, 0, 0, bmpptr);
						}
						//CP_debug_s("Write Image");
						//commandqueue.enqueueWriteImage(inputImage, true, origin2, region2, screenSize.w * 4, 0, bmpptr);
						maxWorkSize = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
						CP_debug_s(("Max work group size : " + to_string(maxWorkSize)).c_str());


						if (restart) {
							restart = false;

							TriglavPlugInInt procResult;
							TriglavPlugInFilterRunProcess(recordSuite, &procResult, hostObject, kTriglavPlugInFilterRunProcessStateStart);
							if (procResult == kTriglavPlugInFilterRunProcessResultExit) break;
							if (filterInfo->amount != 0) {
								blockIndex = 0;
								int am = filterInfo->amount;
								float ar_abs = abs(filterInfo->aspect/100);
								if (filterInfo->aspect < 0) {
									amount_x = am;
									amount_y = (int)floor(am*(1.0 - ar_abs));
								} else {
									amount_x = (int)floor(am*(1.0 - ar_abs));
									amount_y = am;
								}
								isGaussian = filterInfo->blurType;
								sampler = cl::Sampler(context, true, addressModes[filterInfo->sampling], CL_FILTER_LINEAR);

								if (cDiv != filterInfo->divideSise) {
									CP_debug_s("Change Divide");
									int ds = filterInfo->divideSise;
									cDiv = ds;
									divSize = Size2D(ds, ds);
									blocks = CPUtil::calcDividedRects(screenSize, divSize);
									blockNum = blocks.size();
									dstSize.w = (divSize.w > screenSize.w) ? screenSize.w : divSize.w;
									dstSize.h = (divSize.h > screenSize.h) ? screenSize.h : divSize.h;
									bitmapService->createProc(&dstbitmap, dstSize.w, dstSize.h, 4, kTriglavPlugInBitmapScanlineHorizontalLeftTop);
									bitmapService->getAddressProc(&dstpt, dstbitmap, &zeroPos);
									dstbmpptr = (byte*)dstpt;
									allocDstImage = true;
									TriglavPlugInFilterRunSetProgressTotal(recordSuite, pluginServer->hostObject, blockNum);
								}
								if (screenSize.w != srcImage.getImageInfo<CL_IMAGE_WIDTH>() || screenSize.h != srcImage.getImageInfo<CL_IMAGE_HEIGHT>() || allocImage) {
									srcImage = cl::Image2D(context, CL_MEM_USE_HOST_PTR, ioImgFormat, screenSize.w, screenSize.h, 0, bmpptr, &err);
									blocks = CPUtil::calcDividedRects(screenSize, divSize);
									CP_debug_s(("Alloc src image : " + clErrorCode[err]).c_str());
								}
								if (allocDstImage || allocImage) {
									dstImage = cl::Image2D(context, CL_MEM_COPY_HOST_PTR, ioImgFormat, dstSize.w, dstSize.h, 0, dstbmpptr, &err);
									CP_debug_s(("Alloc dst image : " + clErrorCode[err]).c_str());
								}

							}
							else {
								blockIndex = blockRectCount;
								TriglavPlugInFilterRunUpdateDestinationOffscreenRect(recordSuite, hostObject, &selectAreaRect);
							}
						}

						if (blockIndex < blockNum) {
							TriglavPlugInFilterRunSetProgressDone(recordSuite, hostObject, blockIndex);
							TriglavPlugInRect blockRect = blockRects[blockIndex];
							// 対象ブロックの左上の座標の代入
							TriglavPlugInPoint pos;
							pos.x = blockRect.left;
							pos.y = blockRect.top;
							TriglavPlugInRect tRect;




							if (bmpptr != nullptr) {

								if (selectAreaOffscreenObject == nullptr) {
									chrono::system_clock::time_point start, end;
									double dur = 0;
									auto now = chrono::system_clock::now;
									vector<float> xFactor, yFactor;
									xFactor = CPUtil::calcFactor(amount_x, isGaussian);
									yFactor = CPUtil::calcFactor(amount_y, isGaussian);
									cl::Buffer xF(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, xFactor.size() * 4, xFactor.data());
									cl::Buffer yF(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, yFactor.size() * 4, yFactor.data());
									start = now();
									//offscreenService->getBitmapProc(bitmap, &zeroPos, srcOffscreenObject, &zeroPos, screenSize.w, screenSize.h, kTriglavPlugInOffscreenCopyModeNormal);
									TriglavPlugInRect processRect = blocks[blockIndex];
									Bounds2D processBounds = CPUtil::calcBounds(screenSize, processRect, Size2D(0, 0));
									Bounds2D copyBounds = CPUtil::calcBounds(screenSize, processRect, CPUtil::Size2D(amount_x, amount_y));
									cl::size_t<3> copyOrigin = CLFunc::createSize_t({ copyBounds.origin.x, copyBounds.origin.y, 0 });
									cl::size_t<3> copyRegion = CLFunc::createSize_t({ (int)copyBounds.size.w, (int)copyBounds.size.h, 1 });
									cl::size_t<3> copyoOrigin = CLFunc::createSize_t({ processBounds.origin.x, processBounds.origin.y, 0 });
									cl::size_t<3> copyOutOrigin = CLFunc::createSize_t({ processBounds.origin.x - copyBounds.origin.x, processBounds.origin.y - copyBounds.origin.y, 0 });
									cl::size_t<3> copyoRegion = CLFunc::createSize_t({ (int)processBounds.size.w, (int)processBounds.size.h, 1 });
									//cl::size_t<3> copyRegion = CLFunc::createSize_t({ (int)screenSize.w, (int)screenSize.h, 1 });

									inputImage = cl::Image2D(context, CL_MEM_READ_WRITE, ioImgFormat, copyBounds.size.w, copyBounds.size.h);
									outputImage = cl::Image2D(context, CL_MEM_READ_WRITE, ioImgFormat, copyBounds.size.w, copyBounds.size.h);
									tempImage = cl::Image2D(context, CL_MEM_READ_WRITE, tempImgFormat, copyBounds.size.w, copyBounds.size.h);

									err =  commandqueue.enqueueCopyImage(srcImage, inputImage, copyOrigin, origin, copyRegion);
									CP_debug_s(("Copy src image : " + clErrorCode[err]).c_str());
									dur = calcDuration<chrono::nanoseconds>(start, now()) / 1000000;
									CP_debug_s(("Bitmap copy Time : " + to_string(dur) + "ms").c_str());

									int result;
									start = now();

									size_t workSize[2] = {32, floor(maxWorkSize/32)};
									cl::NDRange offset(0, 0, 0);
									cl::NDRange globalSize(ceil(copyBounds.size.w / (float)workSize[0]) * workSize[0], ceil(copyBounds.size.h / (float)workSize[1]) * workSize[1], 1);
									cl::NDRange localSize(workSize[0], workSize[1], 1);
									int srcSize[] = { copyBounds.size.w, copyBounds.size.h };
									int pOrigin[] = { processBounds.origin.x- copyBounds.origin.x, processBounds.origin.y - copyBounds.origin.y };
									int pSize[] = { processBounds.size.w, processBounds.size.h };
									CP_debug_s(("Image Size : " + to_string(outputImage.getImageInfo<CL_IMAGE_WIDTH>()) + " : " + to_string(outputImage.getImageInfo<CL_IMAGE_HEIGHT>())).c_str());
									CP_debug_s(("Process bounds : " + to_string(processBounds.size.w) + " : " + to_string(processBounds.size.h)).c_str());
									CP_debug_s(("Amount X : " + to_string(globalSize[0])).c_str());
									CP_debug_s(("Amount Y : " + to_string(globalSize[1])).c_str());
									CP_debug_s(("Region X : " + to_string(copyoRegion[0])).c_str());
									CP_debug_s(("Region Y : " + to_string(copyoRegion[1])).c_str());
									CP_debug_s(("CB X : " + to_string(copyBounds.size.w)).c_str());
									CP_debug_s(("CB Y : " + to_string(copyBounds.size.h)).c_str());
									CP_debug_s(("Origin X : " + to_string(copyOutOrigin[0])).c_str());
									CP_debug_s(("Origin Y : " + to_string(copyOutOrigin[1])).c_str());
									CP_debug_s(("src : " + to_string(srcSize[0]) + " : " + to_string(srcSize[1])).c_str());
									CP_debug_s(("offset : " + to_string(offset[0]) + " : " + to_string(offset[1]) + " : " + to_string(offset[2])).c_str());
									CP_debug_s(("global size : " + to_string(globalSize[0]) + " : " + to_string(globalSize[1]) + " : " + to_string(globalSize[2])).c_str());
									CP_debug_s(("local size : " + to_string(localSize[0]) + " : " + to_string(localSize[1]) + " : " + to_string(localSize[2])).c_str());

									kernel.setArg(0, inputImage);
									kernel.setArg(1, tempImage);
									kernel.setArg(2, amount_x);
									kernel.setArg(3, srcSize);
									kernel.setArg(4, pOrigin);
									kernel.setArg(5, pSize);
									kernel.setArg(6, 1);
									kernel.setArg(7, sampler);
									kernel.setArg(8, xF);
									result = commandqueue.enqueueNDRangeKernel(kernel, offset, globalSize, localSize);
									kernel.setArg(0, tempImage);
									kernel.setArg(1, outputImage);
									kernel.setArg(2, amount_y);
									kernel.setArg(3, srcSize);
									kernel.setArg(4, pOrigin);
									kernel.setArg(5, pSize);
									kernel.setArg(6, 0);
									kernel.setArg(7, sampler);
									kernel.setArg(8, yF);
									result = commandqueue.enqueueNDRangeKernel(kernel, offset, globalSize, localSize);
									CP_debug_s(("Kernel result : " + clErrorCode[result]).c_str());
									//CP_debug_s("Kernel returned");
									err = commandqueue.enqueueCopyImage(outputImage, dstImage, copyOutOrigin, origin, copyoRegion);
									CP_debug_s(("Copy result image : " + clErrorCode[err]).c_str());
									err = commandqueue.enqueueReadImage(dstImage, true, origin, copyoRegion, dstSize.w*4, 0, dstbmpptr);
									CP_debug_s(("Read result image : " + clErrorCode[err]).c_str());
									dur = calcDuration<chrono::nanoseconds>(start, now()) / 1000000;
									CP_debug_s(("NDRange kernel Time : " + to_string(dur) + "ms").c_str());
									TriglavPlugInPoint copyPos = { processBounds.origin.x, processBounds.origin.y};
									CP_debug_s(("CP X : " + to_string(copyPos.x)).c_str());
									CP_debug_s(("CP Y : " + to_string(copyPos.y)).c_str());
									offscreenService->setBitmapProc(dstOffscreenObject, &copyPos, dstbitmap, &zeroPos, copyoRegion[0], copyoRegion[1], kTriglavPlugInOffscreenCopyModeNormal);
								}
								else {
									TriglavPlugInBitmapObject bitmaps;
									byte *bmpptrs;
									TriglavPlugInPtr pts;
									bitmapService->createProc(&bitmaps, screenSize.w, screenSize.h, 4, kTriglavPlugInBitmapScanlineHorizontalLeftTop);
									offscreenService->getBitmapProc(bitmaps, &zeroPos, selectAreaOffscreenObject, &zeroPos, screenSize.w, screenSize.h, kTriglavPlugInOffscreenCopyModeNormal);
									bitmapService->getAddressProc(&pts, bitmaps, &zeroPos);
									bmpptrs = (byte*)pts;
									CP_debug_s("Copy selected area");/*
									int c = 0;
									for (size_t y = 0; y < screenSize.h; y++) {
										for (size_t x = 0; x < screenSize.w; x++) {
											unsigned int pos = (y*screenSize.w + x);
											simg.data[pos * 4] = 255;
											simg.data[pos * 4 + 1] = 255;
											simg.data[pos * 4 + 2] = 255;
											simg.data[pos * 4 + 3] = bmpptrs[pos*4];
											c += bmpptrs[pos*4] ? 1 : 0;
										}
									}
									cv::imwrite("D:/Dev/selected.png", simg);*/
								}
							}

							TriglavPlugInFilterRunUpdateDestinationOffscreenRect(recordSuite, hostObject, &blocks[blockIndex]);
							CP_debug_s("Update image");
							blockIndex++;

						}
						TriglavPlugInInt procResult;
						if (blockIndex < blockNum) {
							TriglavPlugInFilterRunProcess(recordSuite, &procResult, hostObject, kTriglavPlugInFilterRunProcessStateContinue);
						}
						else {
							CP_debug_s("Finished");

							TriglavPlugInFilterRunSetProgressDone(recordSuite, hostObject, blockIndex);
							TriglavPlugInFilterRunProcess(recordSuite, &procResult, hostObject, kTriglavPlugInFilterRunProcessStateEnd);
						}
						if (procResult == kTriglavPlugInFilterRunProcessResultRestart) {
							CP_debug_s("Restarting");
							restart = true;
						}
						else if (procResult == kTriglavPlugInFilterRunProcessResultExit) {
							CP_debug_s("Exit");
							break;
						}
					}
//					CP_debug_s("確定するね");
					*result = kTriglavPlugInCallResultSuccess;
				}
				break;
			}
			default:
				break;
		}
	}
	return;
}