#include <UtilFunc.hpp>
#include <string>
#include <chrono>
using namespace std;

bool contain(string srcstr, string contstr) {
	return srcstr.find(contstr) != string::npos;
}
