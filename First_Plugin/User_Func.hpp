#pragma once

#include <TriglavPlugInSDK\TriglavPlugInSDK.h>
#include <string>
#include <algorithm>
using namespace std;
inline unsigned char ucclamp(int v) {
	return min(max(0, v), 255);
}

