#pragma once
#include <CL\cl.hpp>
#include <vector>
#include <string>
#include <map>
using namespace std;

namespace CLFunc {
	cl::size_t<3> createSize_t(const int(&v)[3]);

	vector<string> getPlatformNames(vector<cl::Platform> &platforms);

	inline string getPlatformName(cl::Platform &platform);

	inline string getDeviceName(cl::Device &device);

	vector<string> getDeviceNames(vector<cl::Device> &Devices);

	void sortbydGPU(vector<cl::Platform> &platforms);

	cl::Device chooseDevice(vector<cl::Platform> platforms, cl_device_type type);
}

extern map<int, string> clErrorCode;