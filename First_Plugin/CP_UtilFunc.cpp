#include <CP_Util.hpp>
#include <string>
using namespace std;
using namespace CPUtil;

void CPUtil::getStrObj(TriglavPlugInServer *pServer, TriglavPlugInStringObject &strObj, TriglavPlugInInt strID) {
	TriglavPlugInStringService *strService = pServer->serviceSuite.stringService;
	strService->createWithStringIDProc(&strObj, strID, pServer->hostObject);
}

void CPUtil::createStrObj(TriglavPlugInServer *pServer, TriglavPlugInStringObject &strObj, string str) {
	TriglavPlugInStringService *strService = pServer->serviceSuite.stringService;
	strService->createWithLocalCodeStringProc(&strObj, str.c_str(), str.length());
}

Bounds2D CPUtil::calcBounds(Size2D imgSize, TriglavPlugInRect pArea, Size2D extendSize) {
	int left = pArea.left - extendSize.w;
	if (left < 0) {
		left = 0;
	}
	int right = pArea.right + extendSize.w;
	if (right >= imgSize.w) {
		right = imgSize.w - 1;
	}
	int top = pArea.top - extendSize.h;
	if (top < 0) {
		top = 0;
	}
	int bottom = pArea.bottom + extendSize.h;
	if (bottom >= imgSize.h) {
		bottom = imgSize.h - 1;
	}
	Vec2 o(left, top);
	Size2D s(right - left + 1, bottom - top + 1);
	return Bounds2D(o, s);
}

vector<TriglavPlugInRect> CPUtil::calcDividedRects(Size2D srcSize, Size2D divSize, Size2D *block2DNum) {
	vector<TriglavPlugInRect> blocks;
	int xNum = ceil((float)srcSize.w / divSize.w);
	int yNum = ceil((float)srcSize.h / divSize.h);
	int blockNum = xNum * yNum;
	blocks.resize(blockNum);
	if (block2DNum != nullptr) {
		block2DNum->w = xNum;
		block2DNum->h = yNum;
	}
	for (size_t y = 0; y < yNum; y++) {
		for (size_t x = 0; x < xNum; x++) {
			TriglavPlugInRect tRect;
			int blockPos = xNum * y + x;
			int left = divSize.w*x;
			int right = (left + divSize.w > srcSize.w) ? srcSize.w - 1 : left + divSize.w - 1;
			int top = divSize.h*y;
			int bottom = (top + divSize.h > srcSize.h) ? srcSize.h - 1 : top + divSize.h - 1;
			tRect.left = left;
			tRect.right = right;
			tRect.top = top;
			tRect.bottom = bottom;
			blocks[blockPos] = tRect;
		}
	}
	return blocks;
}

vector<float> CPUtil::calcFactor(int blurAmount, bool isGaussian) {
	int totalSize = blurAmount * 2 + 1;
	vector<float> factor(totalSize);
	if (isGaussian) {
		float sum = 0.0;
		float sigma2 = pow(blurAmount / 3.33, 2) * 2;
		for (float i = -blurAmount; i <= blurAmount; i++) {
			float gauss = exp(-(i*i) / (sigma2));
			sum += gauss;
			factor[i+blurAmount] = gauss;
		}
		for (size_t i = 0; i < totalSize; i++) {
			factor[i] /= sum;
		}
	}
	else
	{
		fill(factor.begin(), factor.end(), 1.0 / totalSize);
	}
	return factor;
}