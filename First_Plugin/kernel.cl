// TODO: Add OpenCL kernel code here.
#ifndef OCL_EXTERNAL_INCLUDE
#define OCL_EXTERNAL_INCLUDE(x) #x
#endif

OCL_EXTERNAL_INCLUDE(
__kernel void func(read_only image2d_t src_image, write_only image2d_t dst_image, int l, int2 srcSize, int2 pOrigin, int2 pSize, int ho, sampler_t sampler, __constant float *factor) {
	float w = srcSize.x;
	float h = srcSize.y;
	int2 gid = (int2)(get_global_id(0), get_global_id(1));
	float2 coord = (float2)((gid.x + 0.5) / w, (gid.y + 0.5) / h);
	int2 coordo = gid;
	bool isProcessArea = false;
	if (ho) {
		isProcessArea = gid.x >= pOrigin.x && gid.x<pOrigin.x + pSize.x && gid.y<srcSize.y;
	}
	else {
		isProcessArea = gid.x >= pOrigin.x && gid.x<pOrigin.x + pSize.x && gid.y<srcSize.y && gid.y<pOrigin.y + pSize.y;
	}
	if (isProcessArea) {
		if (l) {
			float4 pixel = 0;
			float alpha = 0;
			float4 divn = 0;
			float2 rpos;
			float2 pitch;
			if (ho) {
				rpos = (float2)(-l / w, 0.0);
				pitch = (float2)(1.0 / w, 0.0);
			}
			else {
				rpos = (float2)(0.0, -l / h);
				pitch = (float2)(0.0, 1.0 / h);
			}
			for (int i = 0; i < l*2+1; i++) {
				float fa = factor[i];
				float4 tcolor = read_imagef(src_image, sampler, coord + rpos);
				alpha += tcolor.w*fa;
				pixel += tcolor * fa*tcolor.w;
				divn += fa * tcolor.w;
				rpos += pitch;
			}
			if (!divn.x) { divn = (float4)(1); }
			pixel /= divn;

			//if(!ho)alpha = 1.0;
			pixel.w = alpha;

			write_imagef(dst_image, coordo, pixel);
		}
		else {
			write_imagef(dst_image, coordo, read_imagef(src_image, sampler, coord));
		}
	}
}
)